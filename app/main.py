import sys

from console.providers.command_provider import CommandProvider

if __name__ == "__main__":
    command = sys.argv[1:]
    command_provider = CommandProvider(command)
    command_provider.provide_command()
