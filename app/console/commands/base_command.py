from console.validators.command_args_validator import CommandArgsValidator


class BaseCommand(object):
    args = []
    parsed_args = {}
    required_args = {}
    optional_args = {}

    def __init__(self, args):
        self.args = args
        self.__parse_args()
        self.__validate_args()

    def __parse_args(self):
        for arg in self.args:
            arg = arg.split('=', 1)
            arg_name = self.__clean_arg_name(self.__get_from_tuple(arg, 0, ''))
            arg_value = self.__parse_arg_value(arg)
            self.parsed_args[arg_name] = arg_value

    def __parse_arg_value(self, arg):
        arg_value = self.__get_from_tuple(arg, 1, '')

        return arg_value.split(',') if ',' in arg_value else arg_value

    def __clean_arg_name(self, arg_name):
        return arg_name.replace('--', '') if self.__is_option(arg_name) else arg_name

    def __validate_args(self):
        command_args_validator = CommandArgsValidator(self, self.parsed_args)

        if not command_args_validator.validate():
            exit()

    @staticmethod
    def __is_option(arg_name):
        return '--' in arg_name

    @staticmethod
    def __get_from_tuple(array, index, default):
        return array[index] if index < len(array) else default
