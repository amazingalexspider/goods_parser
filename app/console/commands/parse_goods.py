from console.providers.parser_provider import ParserProvider
from .base_command import BaseCommand


class ParseGoods(BaseCommand):
    required_args = [
        'good'
    ]
    optional_args = [
        'store',
        'format',
        'output',
        'order',
        'micro',
        'type',
        'mount_type',
        'connection_type'
    ]

    def __init__(self, args):
        super().__init__(args)

    def run(self):
        parser_provider = ParserProvider(self.parsed_args.get('store', ''))
        parsers = parser_provider.provide_parsers()
        models = list()

        for parser in parsers:
            models.append(parser(self.parsed_args).parse())
