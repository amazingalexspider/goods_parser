class CommandArgsValidator:
    parsed_args = {}
    command = None

    def __init__(self, command, parsed_args):
        self.command = command
        self.parsed_args = parsed_args

    def validate(self):
        is_valid = True

        for arg_name, arg_value in self.parsed_args.items():
            if not arg_name or not arg_value:
                is_valid = False
                print('Given data isn\'t correct. Passed {0} = {1}'.format(arg_name, arg_value))

        return self.__has_required_args() and is_valid

    def __has_required_args(self):
        answer = all(arg_name in self.parsed_args for arg_name in self.command.required_args)

        if not answer:
            print('Provided data is not enough!')
            print('Must contain: {0}'.format(self.command.required_args))
            print('Passed: {0}'.format(self.parsed_args))

        return answer

    @staticmethod
    def __has_name(arg):
        return bool(arg)
