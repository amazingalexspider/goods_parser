from citilink.citilink_parser import CitilinkParser


class ParserProvider:
    needed_parsers = []
    possible_parsers = {
        'citilink': CitilinkParser,
    }

    def __init__(self, parsers):
        self.needed_parsers = parsers if isinstance(parsers, tuple) else [parsers]

    def provide_parsers(self):
        parsers = []

        for parser in self.needed_parsers:
            if parser in self.possible_parsers.keys():
                parsers.append(self.possible_parsers[parser])

        return parsers
