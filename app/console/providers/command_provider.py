from console.commands.parse_goods import ParseGoods


class CommandProvider:
    command = None
    possible_commands = {
        'parse:goods': ParseGoods,
    }

    def __init__(self, command):
        self.command = command

    def provide_command(self):
        command_name = self.__get_command_name()
        needed_command = self.possible_commands.get(command_name, None)

        if needed_command is None:
            print('Wrong command signature!')
        else:
            needed_command(self.__get_command_args()).run()

    def __get_command_name(self):
        return next(iter(self.command), None)

    def __get_command_args(self):
        command_args = self.command
        del command_args[0]

        return command_args
