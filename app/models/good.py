class Good:
    __code = 0
    __connection_type = None
    __type = None
    __price = None
    __club_price = None
    __have_in_store = False

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            getattr(self, 'set_' + key)(value)

    def to_dict(self):
        return {
            'code': self.get_code(),
            'connection_type': self.get_connection_type(),
            'type': self.get_type(),
            'price': self.get_price(),
            'club_price': self.get_club_price(),
            'have_in_store': self.get_have_in_store()
        }

    def get_code(self):
        return self.__code

    def get_connection_type(self):
        return self.__connection_type

    def get_type(self):
        return self.__type

    def get_price(self):
        return self.__price

    def get_club_price(self):
        return self.__club_price

    def get_have_in_store(self):
        return self.__have_in_store

    def set_code(self, code):
        self.__code = code

    def set_connection_type(self, connection_type):
        self.__connection_type = connection_type

    def set_type(self, type):
        self.__type = type

    def set_price(self, price):
        self.__price = price

    def set_club_price(self, club_price):
        self.__club_price = club_price

    def set_have_in_store(self, have_in_store):
        self.__have_in_store = have_in_store
