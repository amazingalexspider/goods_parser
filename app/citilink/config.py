config = {
    'base_url': 'https://www.citilink.ru/',
    'category_urls': {
        'headphones': '/mobile/handsfree/'
    },
    'query_parts': {
        'common': {
            'order': {
                'price_asc': 'sorting=price_asc',
                'price_desc': 'sorting=price_desc',
            },
        },
        'headphones': {
            'filters': {
                'available': 'available=1&status=55395790available=1&status=1',
                'micro': {
                    'with': '11965_240',
                    'without': '11966_240'
                },
                'mount_type': {
                    'in_auricle': '7271_240vd1ushnoyd1rakovine',
                    'klipsa': '7271_240klipsa',
                    'behind_ear': '7271_240kreplenied1zad1ukhom',
                    'neck_holder': '7271_240nasheynyyd1derzhatela2',
                    'headband': '7271_240ogolova2e',
                    'neckband': '7271_240sheynyyd1obod',
                },
                'type': {
                    'liners': '7279_240vkladyshi',
                    'monitors': '7279_240monitory',
                    'over_ear': '7279_240nakladnye',
                },
                'connection_type': {
                    'bluetooth': '7281_240besprovodnyed1bluetooth',
                    'wired': '7281_240besprovodnyed1bluetooth',
                }
            },
        },
    }
}
