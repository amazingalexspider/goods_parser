from importlib import import_module


class CitilinkParser:
    url = None
    config = None
    beautifulSoup = None
    parsed_args = {}

    def __init__(self, parsed_args):
        self.parsed_args = parsed_args
        self.config = import_module('citilink.config').config

    def parse(self):
        self.url = self.__build_url()
        print(self.url)

        return []

    def __build_url(self):
        base_url = self.config.get('base_url')

        return base_url + self.__build_query()

    def __build_query(self):
        good = self.parsed_args.get('good')
        good_filters = {
            **self.config.get('query_parts').get(good).get('filters'),
            **self.config.get('query_parts').get('common')
        }
        query = '?' + good_filters.get('available')

        for arg_name, arg_value in self.parsed_args.items():

            if arg_name in good_filters:
                query_filter = self.__make_filter(good_filters.get(arg_name), arg_value)
                query += '&' + query_filter

        return query

    @staticmethod
    def __make_filter(good_filters, arg_value):
        if isinstance(arg_value, list):
            query_filter = [good_filters.get(sub_arg) for sub_arg in arg_value]
        else:
            query_filter = good_filters.get(arg_value, '')

        return 'f=' + ','.join(query_filter) if isinstance(query_filter, list) else query_filter
